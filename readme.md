## EdgeAnimation.js

+ version : v0.1.0
+ author : Nurun R&D
+ description : jQuery plugin for loading Adobe Edge Animate animation.

### key points :

+ Dynamic loading of Adobe Edge Animate animation.
+ Dynamic modification of existing content from an Adobe Edge animation.
+ Animation playback control.

### Usage :

#### Dependencies :

The plugin require the following libraries :

+ JQuery (TBA)
+ Funex (TBA)

How to import those libraries :

``` html
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script src="../funex.js"></script>
 <script src="../EdgeAnimation.js"></script>
 ```

### How-to use :

#### Pulgin initialisation :

``` html
<div class="animationContainer"></div>
 ```

> **id** is required by Edge Animate, if no **id** specified, one will be generated for this specific element.

``` js
$(".animationContainer").edgeAnimation({
    edgePreloadPath: edgePreloadPath,
    autoPlay: true
});
 ```

#### Setup options :

Here the options listing for the setup :

+ **edgePreloadPath** String *(default:null)* - path to the preload file generated by Adobe Edge **Warning : this option is mandatory**.
+ **autoPlay** Boolean *(default:false)* - If **true** launch playback once animation has loaded.
+ **template** Object *(default:null)* - TODO.
+ **data** Object *(default:null)* - TODO.

#### Animation playback :

``` js
var position = 0;
$(".animationContainer").edgeAnimation("play",position);
 ```

#### Animation pausing :

``` js
$(".animationContainer").edgeAnimation("pause");
 ```

### Content modification :

How to inject content on start up :

``` js
$(".animationContainer").edgeAnimation({
    edgePreloadPath: edgePreloadPath,
    autoPlay: true,
    template: {
        "title" : "text(titleText)"
    },
    data {
        "titleText" : "This is a title"
    }
});
 ```

#### Template :

The template manages dependencies between animation items and transformation behavior.

``` js
//...
template: {
    "title" : "text(titleText)"
}
//...
});
 ```

In this example we create a relation between :
+ Animation item on the stage named *title* (first field in the property panel).
+ A *text* action that uses a parameter named *titleText*.

Currently available actions :
+ **text(String)** - modifie the content of a textfield.
+ **color(String)** - modifie the color of a textfield (au format *#f4f4f4*).
+ **fontSize(String)** - modifie the size of a textfield.
+ **image(String)** - modifie teh image path.

> More modifications will be implemented latter on and the option to create custom actions.



It is possible to target elements nested into other elements :

``` js
//...
template: {
    "header.title" : "text(titleText)"
}
//...
});
 ```

In the example, we target the *title* element nested into the *header* (*header* is part of the main stage).

#### Data :

The data *object* contains values that will be used by the *template*.

``` js
//...
template: {
    "title" : "text(titleText)"
},
data {
    "titleText" : "This is a title"
}
//...
 ```

In this example, the template will replace the content from the *title* textfield by the value from the *titleText* attribute of the data object.

#### Data modification afetr launch :

It is possible to modify data once the animation has loaded. Two commands are available :

``` js
$(".animationContainer").edgeAnimation("template", {
        "title" : "text(titleText)"
    });
 ```

The *template* command will modify the template associated to the animation.

``` js
$(".animationContainer").edgeAnimation("data", {
        "titleText" : "This is a title"
    });
 ```

The *template* will modify the data associated with the animation.


### TODO :

+ Grunt setup for project creation.
+ *bower* packaging.
+ Code refactoring (errors handling).
+ Adding tests scenarios.
+ Adding new actions for templates.
+ Adding event handling and dispatching (init, play, pause, stop...).
+ Enable event propagation from the timeline cuepoints.
+ Complete and auto-destruction of the widget and your website.

