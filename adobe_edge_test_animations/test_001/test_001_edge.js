/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'foo',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'test',
            type:'image',
            rect:['200px','-179px','1440px','1086px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"test.jpg",'0px','0px']
         },
         {
            id:'image',
            type:'image',
            rect:['56px','121px','87px','65px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"test.jpg",'0px','0px']
         }],
         symbolInstances: [
         {
            id:'foo',
            symbolName:'Foo'
         }
         ]
      },
   states: {
      "Base State": {
         "${_test}": [
            ["style", "height", '200px'],
            ["style", "top", '0px'],
            ["style", "left", '200px'],
            ["style", "width", '265px']
         ],
         "${_image}": [
            ["style", "top", '121px'],
            ["style", "height", '65px'],
            ["style", "left", '-104px'],
            ["style", "width", '87px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "width", '200px'],
            ["style", "height", '200px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: true,
         labels: {
            "start": 0,
            "yo": 1000,
            "end": 2000
         },
         timeline: [
            { id: "eid14", tween: [ "style", "${_image}", "left", '16px', { fromValue: '-104px'}], position: 1500, duration: 500, easing: "easeOutQuint" },
            { id: "eid19", tween: [ "style", "${_test}", "left", '109px', { fromValue: '200px'}], position: 1750, duration: 250 },
            { id: "eid32", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_foo}', [] ], ""], position: 0 }         ]
      }
   }
},
"Foo": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'Rectangle',
      stroke: [0,'rgba(0,0,0,1)','none'],
      rect: ['0px','0px','200px','81px','auto','auto'],
      fill: ['rgba(192,192,192,1)',[270,[['rgba(177,177,177,1.00)',0],['rgba(255,255,255,0.00)',100]]]]
   },
   {
      font: ['Verdana, Geneva, sans-serif',24,'rgba(255,255,255,1.00)','normal','none',''],
      type: 'text',
      id: 'labelA',
      text: 'label',
      textShadow: ['rgba(0,0,0,0.65)',1,1,4],
      rect: ['16px','17px','167px','33px','auto','auto']
   },
   {
      font: ['Verdana, Geneva, sans-serif',24,'rgba(255,255,255,1.00)','normal','none',''],
      type: 'text',
      id: 'labelB',
      text: 'label',
      textShadow: ['rgba(0,0,0,0.65)',1,1,4],
      rect: ['-184px','50px','167px','33px','auto','auto']
   },
   {
      font: ['Verdana, Geneva, sans-serif',24,'rgba(255,255,255,1.00)','normal','none',''],
      type: 'text',
      id: 'labelC',
      text: 'label',
      textShadow: ['rgba(0,0,0,0.65)',1,1,4],
      rect: ['-184px','83px','167px','33px','auto','auto']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_labelB}": [
            ["subproperty", "textShadow.blur", '4px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["subproperty", "textShadow.offsetV", '1px'],
            ["style", "left", '-183px'],
            ["style", "top", '50px'],
            ["style", "height", '33px'],
            ["style", "font-family", 'Verdana, Geneva, sans-serif'],
            ["subproperty", "textShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${symbolSelector}": [
            ["style", "height", '200px'],
            ["style", "width", '200px']
         ],
         "${_Rectangle}": [
            ["color", "background-color", 'rgba(192,192,192,0.00)'],
            ["style", "height", '200px'],
            ["gradient", "background-image", [270,[['rgba(58,58,58,1.00)',0],['rgba(255,255,255,0.00)',100]]]],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_labelC}": [
            ["subproperty", "textShadow.blur", '4px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["subproperty", "textShadow.offsetV", '1px'],
            ["style", "left", '-184px'],
            ["style", "top", '83px'],
            ["style", "height", '33px'],
            ["style", "font-family", 'Verdana, Geneva, sans-serif'],
            ["subproperty", "textShadow.color", 'rgba(0,0,0,0.648438)']
         ],
         "${_labelA}": [
            ["subproperty", "textShadow.blur", '4px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["subproperty", "textShadow.offsetV", '1px'],
            ["style", "left", '-184px'],
            ["style", "top", '17px'],
            ["style", "height", '33px'],
            ["style", "font-family", 'Verdana, Geneva, sans-serif'],
            ["subproperty", "textShadow.color", 'rgba(0,0,0,0.65)']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1500,
         autoPlay: false,
         labels: {
            "start": 0,
            "a": 500,
            "b": 1000,
            "stop": 1500
         },
         timeline: [
            { id: "eid9", tween: [ "style", "${_labelB}", "left", '16px', { fromValue: '-183px'}], position: 500, duration: 500, easing: "easeOutCubic" },
            { id: "eid11", tween: [ "style", "${_labelC}", "left", '16px', { fromValue: '-184px'}], position: 1000, duration: 500, easing: "easeOutCubic" },
            { id: "eid7", tween: [ "style", "${_labelA}", "left", '16px', { fromValue: '-184px'}], position: 0, duration: 500, easing: "easeOutCubic" },
            { id: "eid4", tween: [ "gradient", "${_Rectangle}", "background-image", [270,[['rgba(58,58,58,1.00)',0],['rgba(255,255,255,0.00)',100]]], { fromValue: [270,[['rgba(58,58,58,1.00)',0],['rgba(255,255,255,0.00)',100]]]}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "color", "${_Rectangle}", "background-color", 'rgba(192,192,192,0.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(192,192,192,0.00)'}], position: 0, duration: 0 },
            { id: "eid3", tween: [ "style", "${_Rectangle}", "height", '200px', { fromValue: '200px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-9033193");
