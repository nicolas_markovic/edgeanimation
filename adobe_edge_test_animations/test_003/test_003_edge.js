/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
   fonts['advent-pro, sans-serif']='<script src=\"http://use.edgefonts.net/advent-pro:n2,n5,n7,n4,n1,n6,n3:all.js\"></script>';


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'background',
            type:'image',
            rect:['0px','-45px','579px','290px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"monkey1.jpg",'0px','0px']
         },
         {
            id:'Rectangle',
            type:'rect',
            rect:['0px','0px','500px','200px','auto','auto'],
            fill:["rgba(192,192,192,0.00)",[0,[['rgba(0,0,0,0.81)',0],['rgba(0,0,0,0.00)',81]]]],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Rectangle2',
            type:'rect',
            rect:['493px','2px','5px','196px','auto','auto'],
            opacity:1,
            fill:["rgba(255,255,255,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         },
         {
            id:'title',
            type:'text',
            rect:['14px','9px','472px','65px','auto','auto'],
            text:"TITLE",
            font:['advent-pro, sans-serif',60,"rgba(0,0,0,1)","normal","none",""]
         },
         {
            id:'text',
            type:'text',
            rect:['-246px','76px','236px','108px','auto','auto'],
            text:"text",
            align:"left",
            font:['Arial, Helvetica, sans-serif',14,"rgba(0,0,0,1)","normal","none","normal"]
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_text}": [
            ["style", "top", '76px'],
            ["style", "opacity", '0'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "width", '236px'],
            ["style", "height", '108px'],
            ["style", "font-family", 'Arial, Helvetica, sans-serif'],
            ["style", "left", '-118px'],
            ["style", "font-size", '14px']
         ],
         "${_Rectangle2}": [
            ["style", "top", '2px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '0px'],
            ["style", "left", '493px'],
            ["style", "width", '5px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '500px'],
            ["style", "height", '200px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Rectangle}": [
            ["style", "top", '0px'],
            ["style", "height", '200px'],
            ["gradient", "background-image", [0,[['rgba(0,0,0,0.81)',0],['rgba(0,0,0,0.00)',81]]]],
            ["color", "background-color", 'rgba(192,192,192,0.00)'],
            ["style", "width", '500px']
         ],
         "${_background}": [
            ["style", "top", '-45px'],
            ["transform", "scaleY", '1'],
            ["transform", "scaleX", '1'],
            ["style", "height", '290px'],
            ["style", "left", '-79px'],
            ["style", "width", '579px']
         ],
         "${_title}": [
            ["style", "top", '9px'],
            ["style", "opacity", '0'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "width", '472px'],
            ["style", "height", '65px'],
            ["style", "font-family", 'advent-pro, sans-serif'],
            ["style", "left", '-235px'],
            ["style", "font-size", '60px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         timeline: [
            { id: "eid36", tween: [ "color", "${_text}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1.00)'}], position: 7723, duration: 0 },
            { id: "eid15", tween: [ "style", "${_text}", "left", '14px', { fromValue: '-118px'}], position: 500, duration: 1000, easing: "easeOutSine" },
            { id: "eid34", tween: [ "style", "${_background}", "left", '-9px', { fromValue: '-79px'}], position: 0, duration: 10000 },
            { id: "eid43", tween: [ "style", "${_Rectangle2}", "height", '196px', { fromValue: '0px'}], position: 0, duration: 10000 },
            { id: "eid41", tween: [ "style", "${_text}", "opacity", '1', { fromValue: '0'}], position: 500, duration: 1000 },
            { id: "eid13", tween: [ "style", "${_title}", "left", '14px', { fromValue: '-235px'}], position: 0, duration: 1000, easing: "easeOutSine" },
            { id: "eid35", tween: [ "color", "${_title}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1.00)'}], position: 7723, duration: 0 },
            { id: "eid39", tween: [ "style", "${_title}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 1000 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-5897080");
