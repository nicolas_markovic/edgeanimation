//EdgeAnimate Version 1.5.0.217.23270
(function ($, document, rootScope) {
    //TODO check jQuery conflicts with Adobe Edge
    //TODO handle animation removing...

    var BOOTSTRAP_COMPLETE = "bootstrap_complete";

    //UTILITY FUNCTIONS

    /**
     * @param {Array} data
     * @param {Function} matcher
     * @return {EdgeAnimation | null}
     */
    function search(data, matcher) {
        var dataLength = data.length;
        var result = null;
        for (var i = 0; i < dataLength; i++) {
            var item = data[i];
            if (matcher(item)) {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * @return {string}
     */
    function generateUID() {
        return new Date().getTime().toString(26) + "_" + (Math.random() * 1000 >> 0).toString(26);
    }

    /**
     * @param {String} url
     * @param {Function} callback
     */
    function loadScript(url, callback) {
        //TODO handle errors...
        $.ajax({
            url: url,
            dataType: "text"//mandatory, otherwise script is executed...
        }).done(function (data) {
                callback(data);
            });
    }

    function replaceLoadingScriptsFunction(input, edgeAnimationUID) {
        var regExp = /AdobeEdge\.yepnope\(([^)]+)\)/g;
        var result = regExp.exec(input);
        var parameterName = result[1];
        //TODO comment REGEXP
        //TODO handle REGEXP failure...
        input = input.replace(/(AdobeEdge.requestResources ?\|\| ?function ?\([\w]+, ?[\w]+)(\))/g, "$1,edgeAnimationUID$2");
        input = input.replace(/(AdobeEdge.requestResources\()([\w]+)(\.[\w]+, ?[\w\.]+)(\))/g, "$1$2$3,$2.wrapperUID$4");
        input = input.replace(/(AdobeEdge.requestResources\([\w\.]+, ?[\w.]+)(\))/g, "$1,'" + edgeAnimationUID + "'$2");
        input = input.replace(/({files: ?[\w]+, ?callback: ?[\w]+)(})/g, "$1,wrapperUID:'" + edgeAnimationUID + "'$2");
        input = input.replace(regExp, "window.EdgeAnimation.loadScripts(" + parameterName + ", edgeAnimationUID)");
        return input;
    }

    function addWindowsLoadCallbackTrigger(input) {
        //TODO handle REGEXP failure...
        return input.replace(/(}\)\("\w+"\);)/g, "onDocLoaded();$1");
    }

    function injectScriptIntoDOM(script) {
        //TODO fix IE9
        var scriptElement = document.createElement("script");
        scriptElement.type = "text/javascript";
        scriptElement.text = script;
        document.body.appendChild(scriptElement);
    }

    function loadScriptViaScriptTag(scriptPath, completeCallback) {
        //TODO handle failure
        var scriptElement = document.createElement("script");
        scriptElement.onload = scriptElement.onreadystatechange = function () {
            if (!scriptElement.readyState || scriptElement.readyState == "loaded" || scriptElement.readyState == "complete") {
                scriptElement.onload = scriptElement.onreadystatechange = null;
                if (completeCallback)completeCallback.call(null);
            }
        };
        scriptElement.src = scriptPath;
        document.body.appendChild(scriptElement);
    }

    function updateFilesPaths(input, path, uid) {
        //TODO handle REGEXP failure...
        input = input.replace(/(load: ?")([^"]*edge[^"]*)(")/g, "$1" + path + "$2$3");
        input = input.replace(/(load: ?")([^"]*_edge\.js[^"]*)(")/g, "$1$2?uid=" + uid + "$3");
        input = input.replace(/(load: ?")([^"]*_edgeActions\.js[^"]*)(")/g, "$1$2?uid=" + uid + "$3");
        return input;
    }

    function insertCompID(input, compID) {
        //TODO handle REGEXP failure...
        return input.replace(/(\(")([^"]+)("\);)$/g, "$1" + compID + "$3");
    }

    function hackEdgeAnimationPreloadScript(path, compID, uid, callback) {
        return function (edgePreloadScript) {
            edgePreloadScript = replaceLoadingScriptsFunction(edgePreloadScript, uid);
            edgePreloadScript = updateFilesPaths(edgePreloadScript, path, uid);
            edgePreloadScript = insertCompID(edgePreloadScript, compID);
            edgePreloadScript = addWindowsLoadCallbackTrigger(edgePreloadScript);
            callback(edgePreloadScript);
        }
    }

    function getEdgeAnimationPreloadScriptHackingComplete(edgeBootstrapCallback) {
        getEdgeAnimationPreloadScriptHackingComplete = function () {
            return function (edgePreloadScript) {
                injectScriptIntoDOM(edgePreloadScript);
            };
        };
        //only on first call to this function : a global Edge Bootstrap callback will be added
        return function (edgePreloadScript) {
            injectScriptIntoDOM(edgePreloadScript);
            //at this time AdobeEdge is available the first preload script is loaded and inserted into the DOM.
            AdobeEdge.bootstrapCallback(edgeBootstrapCallback);
        };
    }

    function adobeEdgeBootstrap(compIds) {
        //added a timeout in order to break the current call stack
        //AdobeEdge code will catch all exception that will occur in this call stack
        //I don't want this behavior
        setTimeout(function () {
            $.each(compIds, function (key, compId) {
                var composition = AdobeEdge.getComposition(compId);
                var edgeAnimation = EdgeAnimation.getEdgeAnimationByCompID(compId);
                edgeAnimation.bootstrapComplete(composition);
            });
        }, 0);
    }

    function bootEdgeAnimation(path, preloadScriptFileName, compID, uid) {
        loadScript(path + preloadScriptFileName,
            hackEdgeAnimationPreloadScript(path, compID, uid, getEdgeAnimationPreloadScriptHackingComplete(adobeEdgeBootstrap)));
    }


    function forEachChildren(symbol, func) {
        var children = symbol.getChildSymbols();
        var childCount = children.length;
        for (var i = 0; i < childCount; i++) {
            var child = children[i];
            func.call(null, child);
            forEachChildren(child, func);
        }
    }

    function getSymbolAutoPlayStatus(symbol) {
        return symbol.timelines["Default Timeline"].autoPlay;
    }

    function EdgeAnimationBootState() {
    }

    EdgeAnimationBootState.prototype.playing = false;
    EdgeAnimationBootState.prototype.playStartPosition = 0;
    EdgeAnimationBootState.prototype.isPlaying = function () {
        return this.playing;
    };

    EdgeAnimationBootState.prototype.getPlayStartPosition = function () {
        return this.playStartPosition;
    };

    EdgeAnimationBootState.prototype.play = function (position) {
        this.playing = true;
        this.playStartPosition = position || 0;
    };
    EdgeAnimationBootState.prototype.pause = function () {
        this.playing = false;
    };

    function EdgeAnimationReadyState(edgeStage, previousState) {
        this.edgeStage = edgeStage;
        this.playing = previousState.isPlaying();
        this.playStartPosition = previousState.getPlayStartPosition();
        var edgeAnimationAutoPlay = getSymbolAutoPlayStatus(this.edgeStage);
        switch (true) {
            case !edgeAnimationAutoPlay && this.playing:
            case edgeAnimationAutoPlay && this.playing:
                this.play(this.playStartPosition);
                break;
            case edgeAnimationAutoPlay && !this.playing:
                this.edgeStage.stop();
                forEachChildren(edgeStage, function (symbol) {
                    symbol.stop();
                });
                break;
            case !edgeAnimationAutoPlay && !this.playing:
            default :
        }
    }

    EdgeAnimationReadyState.prototype.playing = false;
    EdgeAnimationReadyState.prototype.playStartPosition = 0;
    EdgeAnimationReadyState.prototype.isPlaying = function () {
        return this.playing;
    };

    EdgeAnimationReadyState.prototype.getPlayStartPosition = function () {
        return this.playStartPosition;
    };
    EdgeAnimationReadyState.prototype.edgeStage = null;
    EdgeAnimationReadyState.prototype.play = function (position) {
        this.playing = true;
        this.playStartPosition = position || 0;
        this.edgeStage.play(position);
    };
    EdgeAnimationReadyState.prototype.pause = function () {
        this.playing = false;
        this.edgeStage.stop();
    };

    function extractPath(value) {
        return value.replace(/[^/]+$/g, "");
    }

    function extractPreloadFileName(value) {
        var result = value.match(/[^/]+$/g);
        return result[0];
    }

    function forEachChildren(symbol, func) {
        var children = symbol.getChildSymbols();
        var childCount = children.length;
        for (var i = 0; i < childCount; i++) {
            var child = children[i];
            func(child);
            forEachChildren(child, func);
        }
    }

    function EdgeAnimation(compID, preloadScriptURL, autoplay) {
        var path = extractPath(preloadScriptURL);
        var preloadScriptFileName = extractPreloadFileName(preloadScriptURL);
        this.dispatcher = $("<dispatcher/>");
        this.compID = compID;
        this.path = path;
        this.uid = generateUID();
        EdgeAnimation.registerEdgeAnimationAnimation(this);
        this.state = new EdgeAnimationBootState();
        if (!!autoplay) {
            this.state.play(0);
        }
        bootEdgeAnimation(path, preloadScriptFileName, this.compID, this.uid)
    }

    EdgeAnimation.prototype.uid = null;
    EdgeAnimation.prototype.compID = null;
    EdgeAnimation.prototype.path = null;
    EdgeAnimation.prototype.dispatcher = null;
    EdgeAnimation.prototype.edgeComposition = null;
    EdgeAnimation.prototype.edgeStage = null;
    EdgeAnimation.prototype.edgeStageElement = null;
    EdgeAnimation.prototype.bootstrappedFlag = false;
    EdgeAnimation.prototype.state = null;

    EdgeAnimation.prototype.addBootstrapCompleteListener = function (callback, context) {
        this.dispatcher.bind(BOOTSTRAP_COMPLETE, $.proxy(callback, context || null));
    };
    EdgeAnimation.prototype.removeBootstrapCallback = function (callback, context) {
        this.dispatcher.unbind(BOOTSTRAP_COMPLETE, $.proxy(callback, context || null));
    };
    EdgeAnimation.prototype.bootstrapComplete = function (edgeComposition) {
        this.edgeComposition = edgeComposition;
        this.edgeStage = this.edgeComposition.getStage();
        this.edgeStageElement = this.edgeStage.getSymbolElement();
        this.bootstrappedFlag = true;
        this.state = new EdgeAnimationReadyState(this.edgeStage, this.state);
        this.dispatcher.trigger(BOOTSTRAP_COMPLETE);
    };
    EdgeAnimation.prototype.isBootstrapped = function () {
        return this.bootstrappedFlag;
    }
    EdgeAnimation.prototype.getStageElement = function () {
        return this.edgeStageElement;
    };
    EdgeAnimation.prototype.getID = function () {
        return this.edgeStageElement ? this.edgeStageElement.attr("id") : null;
    };
    EdgeAnimation.prototype.getCompId = function () {
        return this.compID;
    };

    EdgeAnimation.prototype.play = function (position) {
        this.state.play(position);
    };
    EdgeAnimation.prototype.pause = function () {
        this.state.pause();
    };

    EdgeAnimation.prototype.dispose = function () {
        //TODO
    };

    /**
     * @private
     * @type {Array}
     */
    EdgeAnimation.animations = [];

    /**
     * @private
     * @param {EdgeAnimation} edgeAnimation
     */
    EdgeAnimation.registerEdgeAnimationAnimation = function (edgeAnimation) {
        //TODO check if animation is already registered
        EdgeAnimation.animations.push(edgeAnimation);
    };


    EdgeAnimation.getEdgeAnimationByUID = function (uid) {
        return search(EdgeAnimation.animations, function (animation) {
            return animation.uid === uid;
        });
    };

    EdgeAnimation.getEdgeAnimationByCompID = function (compID) {
        return search(EdgeAnimation.animations, function (animation) {
            return animation.compID === compID;
        });
    };


    EdgeAnimation.loadScripts = function (data, edgeAnimationUID) {
        function loadNextScript(data) {
            if (data.length <= 0) {
                return;
            }
            var scriptData = data.shift();
            switch (true) {
                case !!scriptData.load.match(/jquery/gi) :
                    //TODO handle JQuery conflicts...
                    /*
                     loadScriptViaScriptTag(scriptData.load, function () {
                     scriptData.callback(scriptData.load, false, 0);
                     });
                     */
                    loadNextScript(data);
                    break;
                case !!scriptData.load.match(/edge[0-9.]+min.js/gi):
                    loadScriptViaScriptTag(scriptData.load, function () {
                        scriptData.callback(scriptData.load);
                        loadNextScript(data);
                    });
                    break;
                case !!scriptData.load.match(/_edge.js/gi):
                    loadScript(scriptData.load, function (script) {
                        //TODO handle REGEXP failure...
                        script = script.replace(/(var im ?= ?')(\w+\/)(')/g, "$1" + edgeAnimation.path + "$2$3");
                        script = script.replace(/(\(jQuery, ?AdobeEdge, ?")([^"]+)("\);)/g, "$1" + edgeAnimation.compID + "$3");
                        injectScriptIntoDOM(script);
                        scriptData.callback(scriptData.load);
                        loadNextScript(data);
                    });
                    break;
                case !!scriptData.load.match(/_edgeActions.js/gi):
                    loadScript(scriptData.load, function (script) {
                        //TODO handle REGEXP failure...
                        script = script.replace(/(\(jQuery, ?AdobeEdge, ?")([^"]+)("\);)/g, "$1" + edgeAnimation.compID + "$3");
                        injectScriptIntoDOM(script);
                        scriptData.callback(scriptData.load);
                        loadNextScript(data);
                    });
                    break;
                default :
                    break;
            }
        }

        var edgeAnimation = EdgeAnimation.getEdgeAnimationByUID(edgeAnimationUID);
        loadNextScript(data);
    };

    rootScope.EdgeAnimation = rootScope.EdgeAnimation || EdgeAnimation;


})(jQuery, document, window);


(function ($, rootScope, funex) {
    function createImageFunction(element) {
        return function (src) {
            element.css("background", "url('" + src + "')");
            return this;
        };
    }

    function createTextFunction(element) {
        return function (value) {
            element.html(value);
            return this;
        };
    }

    function createColorFunction(element) {
        return function (value) {
            element.css("color", value);
            return this;
        };
    }

    function createFontSizeFunction(element) {
        return function (value) {
            element.css("font-size", value);
            return this;
        };
    }

    function process(data, template, animationID) {
        function buildJQuerySelector(input, prefix) {
            input = input.replace(/\./g, "_");
            return "#" + prefix + "_" + input;
        }

        $.each(template, function (target, expression) {
            expression = funex(expression);
            var jQuerySelector = buildJQuerySelector(target, animationID);
            $(jQuerySelector).each(function () {
                var element = $(this);
                var context = {};
                context.image = createImageFunction(element);
                context.text = createTextFunction(element);
                context.color = createColorFunction(element);
                context.fontSize = createFontSizeFunction(element);
                expression([context, data]);
            });
        });

    }

    function EdgeAnimationTransformer(edgeAnimation, template, data) {
        this.edgeAnimation = edgeAnimation;
        this.template = template || null;
        this.data = data || null;
        if (edgeAnimation.isBootstrapped()) {
            //edge animation is already bootstrapped
            this.process = process;
            this.process();
        } else {
            edgeAnimation.addBootstrapCompleteListener(function () {
                edgeAnimation.removeBootstrapCallback(arguments.callee);
                this.process = process;
                if (this.data && this.template) {
                    this.process(this.data, this.template, this.edgeAnimation.getID());
                }
            }, this);
        }
    }

    EdgeAnimationTransformer.prototype.edgeAnimation = null;
    EdgeAnimationTransformer.prototype.data = null;
    EdgeAnimationTransformer.prototype.template = null;
    EdgeAnimationTransformer.prototype.process = function () {
    };

    EdgeAnimationTransformer.prototype.setTemplate = function (template) {
        this.transform(this.data, template);
    };

    EdgeAnimationTransformer.prototype.setData = function (data) {
        this.transform(data, this.template);
    };

    EdgeAnimationTransformer.prototype.transform = function (data, template) {
        //TODO check data for reserved key words...
        this.data = data || this.data;
        this.template = template || this.template;
        this.process(this.data, this.template, this.edgeAnimation.getID());
    };

    rootScope.EdgeAnimationTransformer = rootScope.EdgeAnimationTransformer || EdgeAnimationTransformer;

})(jQuery, window, funex);

(function ($, rootScope) {
    var compIDCounter = 0;

    function generateUniqueCompID() {
        return "comp_" + (compIDCounter++) + "_" + new Date().getTime().toString(26) + "_" + (Math.random() * 1000 >> 0).toString(26);
    }

    var OPTIONS = {
        edgePreloadPath: null,
        autoPlay: false,
        template: null,
        data: null
    };

    var EDGE_ANIMATION = "edgeAnimation";
    var EDGE_ANIMATION_TRANSFORMER = "edgeAnimationTransformer";

    var EdgeAnimation = rootScope.EdgeAnimation;
    var EdgeAnimationTransformer = rootScope.EdgeAnimationTransformer;

    function initializeEdgeAnimation(options) {
        var element = $(this);
        var edgeAnimation = element.data(EDGE_ANIMATION);
        var edgeAnimationTransformer = element.data(EDGE_ANIMATION_TRANSFORMER);
        options = $.extend({}, OPTIONS, options);
        if (!edgeAnimation) {
            var compID = generateUniqueCompID();
            element.addClass(compID);
            element.attr("id", element.attr("id") || compID);
            edgeAnimation = new EdgeAnimation(
                compID,
                options.edgePreloadPath,
                options.autoPlay);
            element.data(EDGE_ANIMATION, edgeAnimation);
        }

        if (!edgeAnimationTransformer) {
            edgeAnimationTransformer = new EdgeAnimationTransformer(edgeAnimation, options.template, options.data);
            element.data(EDGE_ANIMATION_TRANSFORMER, edgeAnimationTransformer);
        }

    }

    function playEdgeAnimation(position) {
        var element = $(this);
        var edgeAnimation = element.data(EDGE_ANIMATION);
        if (edgeAnimation) {
            edgeAnimation.play(position);
        } else {
            $.error("EdgeAnimation not found.");
        }
    }

    function pauseEdgeAnimation() {
        var element = $(this);
        var edgeAnimation = element.data(EDGE_ANIMATION);
        if (edgeAnimation) {
            edgeAnimation.pause();
        } else {
            $.error("EdgeAnimation not found.");
        }
    }

    function setEdgeAnimationData(data) {
        var element = $(this);
        var edgeAnimationTransformer = element.data(EDGE_ANIMATION_TRANSFORMER);
        if (edgeAnimationTransformer) {
            edgeAnimationTransformer.setData(data);
        } else {
            $.error("EdgeAnimationTransformer not found.");
        }
    }

    function setEdgeAnimationTemplate(template) {
        var element = $(this);
        var edgeAnimationTransformer = element.data(EDGE_ANIMATION_TRANSFORMER);
        if (edgeAnimationTransformer) {
            edgeAnimationTransformer.setTemplate(template);
        } else {
            $.error("EdgeAnimationTransformer not found.");
        }
    }

    var methods = {
        init: initializeEdgeAnimation,
        play: playEdgeAnimation,
        pause: pauseEdgeAnimation,
        data: setEdgeAnimationData,
        template: setEdgeAnimationTemplate
    };

    $.fn.edgeAnimation = function (method) {
        var args = arguments;
        return this.each(function () {
            if (methods[method]) {
                return methods[ method ].apply(this, Array.prototype.slice.call(args, 1));
            } else if (typeof method === "object" || !method) {
                return methods.init.apply(this, args);
            } else {
                $.error("Method " + method + " does not exist on jQuery.edgeAnimation");
            }
        });

    };

    rootScope.EDGE_ANIMATION = rootScope.EDGE_ANIMATION || EDGE_ANIMATION;


})(jQuery, window);
